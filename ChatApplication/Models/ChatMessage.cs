﻿using ChatApplication.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication.Models
{
    public class ChatMessage
    {
        public async static Task<bool> RemoveMessage(int messageId)
        {
            if (await User.GetUserRole() != UserRole.Admin)
                return false;

            var message = await Main.dbContext.ChatMessages
                .Where(c => c.Id == messageId)
                .FirstOrDefaultAsync();
            if (message == null)
                return false;

            message.DeletedAt = Main.GetCurrentUnixTimestamp();
            await Main.dbContext.SaveChangesAsync();

            return true;
        }

        public async static Task<bool> RemoveUserMessages(Guid userId)
        {
            if (await User.GetUserRole() != UserRole.Admin)
                return false;

            var messages = await Main.dbContext.ChatMessages
                .Where(c => c.UserId == userId)
                .ToListAsync();
            
            foreach(var message in messages)
            {
                message.DeletedAt = Main.GetCurrentUnixTimestamp();
            }

            await Main.dbContext.SaveChangesAsync();

            return true;
        }
    }
}
