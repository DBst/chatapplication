﻿using ChatApplication.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication.Models
{
    internal class Main
    {
        public static ChatDbContext dbContext { get; set; }

        public Main()
        {
            dbContext = new ChatDbContext();
        }

        public static int GetCurrentUnixTimestamp()
        {
            return (int) DateTimeOffset.Now.ToUnixTimeSeconds();
        }

        public static string ConvertUnixToTime(int unixTimestamp)
        {
            DateTime dateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dateTime = dateTime.AddSeconds(unixTimestamp).ToLocalTime();

            string time = dateTime.ToString("HH:mm:ss");
            return time;
        }
    }
}
