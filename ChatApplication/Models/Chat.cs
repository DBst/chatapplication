﻿using ChatApplication.Entities;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ChatApplication.Forms.Chats;
using System.Windows.Forms;
using ChatApplication.Models.Dto;
using System.Web.UI.WebControls;
using Panel = System.Windows.Forms.Panel;

namespace ChatApplication.Models
{
    public class Chat
    {
        public Guid Id { get; set; }
        public ChatType Type { get; set; }
        public string Name { get; set; }
        public int LastLoadedId { get; set; }
        public static Task LoadMessagesThread { get; set; }
        public static Task OnlineUsersThread { get; set; }
        private static string _errorMessage { get; set; }

        public async Task<IEnumerable<ChatMessageEntity>> GetMessages(ChatDbContext dbContext)
        {
            var messages = await dbContext.ChatMessages
                .Include(cm => cm.User)
                .Where(cm => cm.ChatId == Id && cm.Id > LastLoadedId)
                .OrderBy(cm => cm.AddedAt)
                .ToListAsync();

            var lastMessage = messages.LastOrDefault();
            if (lastMessage != null)
                LastLoadedId = lastMessage.Id;

            return messages;
        }

        public async Task<IEnumerable<ChatMessageEntity>> GetRecentlyDeletedMessages(ChatDbContext dbContext)
        {
            var deletedMessages = await dbContext.ChatMessages
                .Where(cm => cm.ChatId == Id && cm.DeletedAt >= Main.GetCurrentUnixTimestamp() - 1)
                .ToListAsync();

            return deletedMessages;
        }

        public static async Task RefreshChats()
        {
            var chats = await Chat.GetUserChats();

            var chatList = GetFieldByName(User.Parent.Controls, "ChatList");
            chatList.Controls.Clear();
            chatList.Controls.Add(ChatItem.GetChatItem("Dashboard", ChatType.Dashboard, Guid.Empty));

            foreach (var chat in chats)
            {
                var chatItem = ChatItem.GetChatItem(chat.Name, chat.Type, chat.Id);
                chatList.Controls.Add(chatItem);
            }

        }

        public static Control GetFieldByName(Control.ControlCollection controls, string value)
        {
            Control field = null;
            foreach (Control control in controls)
            {
                if (control.Name == value)
                {
                    field = control;
                    break;
                }
                else if (control.GetType() == typeof(Panel))
                {
                    var panel = (Panel)control;
                    var newControls = panel.Controls;
                    field = GetFieldByName(newControls, value);
                    break;
                }
            }
            return field;
        }

        public async Task<List<UserDto>> GetUsersList()
        {
            var dbContext = new ChatDbContext();

            List<UserEntity> usersData = new List<UserEntity>();
            if (Type == ChatType.PublicChat)
                usersData = await dbContext.Users.ToListAsync();
            else if (Type == ChatType.PrivateChat)
            {
                var usersInPrivateChat = await dbContext.UserAccesses
                    .Where(cu => cu.ChatId == Id)
                    .Include(cu => cu.User)
                    .ToListAsync();
                usersData = usersInPrivateChat.Select(u => u.User).ToList();
            }
            else
                return null;

            List<UserDto> users = new List<UserDto>();
            foreach (var user in usersData)
            {
                users.Add(new UserDto()
                {
                    Id = user.Id,
                    Username = user.Username,
                    Email = user.Email,
                    RecentlyOnlineAt = user.RecentlyOnlineAt,
                });
            }

            return users;
        }

        public static async void OpenView(object sender, EventArgs e = null)
        {
            ChatItem item = sender as ChatItem;

            StopInterval();

            if (User.SelectedChat != null && User.SelectedChat.Id == item.Id)
                return;

            Form view = await PrepareView(item);
            if (view == null)
                return;

            User.Parent.GetOfflineUsersListBox().Items.Clear();
            User.Parent.GetOnlineUsersListBox().Items.Clear();

            DisposeTasks();
            SetViewProperties(view);
        }

        private async static Task<Form> PrepareView(ChatItem item)
        {
            Form view;

            if (item.Type == ChatType.PublicChat || item.Type == ChatType.PrivateChat)
            {
                var chat = new Chat()
                {
                    Id = item.Id,
                    Type = item.Type,
                    Name = item.Name,
                };
                if (item.Type == ChatType.PrivateChat)
                {
                    if (!await Security.HasAccessToChat(chat))
                        return null;
                }
                User.SelectedChat = chat;
                view = new ChatTemplate(chat);
            }
            else if (item.Type == ChatType.Dashboard)
            {
                User.SelectedChat = null;
                view = new Dashboard();
            }
            else
                throw new Exception("Cannot find view!");

            return view;
        }

        public static void ChangeMessageFieldSize(int maxSize)
        {
            var child = User.Parent.ActiveMdiChild;
            var messages = Chat.GetFieldByName(child.Controls, "MessagesBox");
            if (messages == null)
                return;

            foreach (var control in messages.Controls)
            {
                var message = (MessageItem)control;
                var messageField = Chat.GetFieldByName(message.Controls, "MessageField");
                var messageWidth = maxSize;

                messageField.MaximumSize = new System.Drawing.Size(messageWidth, 0);
            }
        }

        private static void SetViewProperties(Form view)
        {
            view.MdiParent = Application.OpenForms[0];
            view.TopLevel = false;
            view.MinimizeBox = false;
            view.MaximizeBox = false;
            view.WindowState = FormWindowState.Normal;
            view.FormBorderStyle = FormBorderStyle.None;
            view.Dock = DockStyle.Fill;
            view.ControlBox = false;
            view.ShowIcon = false;
            view.ShowInTaskbar = false;
            view.Show();
        }

        private static void DisposeTasks()
        {
            if (LoadMessagesThread != null && LoadMessagesThread.IsCompleted)
                LoadMessagesThread.Dispose();
            if (OnlineUsersThread != null && OnlineUsersThread.IsCompleted)
                OnlineUsersThread.Dispose();
        }

        public static void StopInterval()
        {
            if (User.Parent.ActiveMdiChild == null)
                return;

            var form = User.Parent.ActiveMdiChild;
            var chat = form as ChatTemplate;

            if (chat != null)
                chat.RunInterval = false;
        }

        public async static Task<IEnumerable<ChatEntity>> GetUserChats()
        {
            var user = await User.GetUserBySession();
            var userId = user.Id;

            var publicChats = await Main.dbContext.Chats
                .Where(c => c.Type == ChatType.PublicChat)
                .ToListAsync();
            var userChats = await Main.dbContext.UserAccesses
                .Where(cu => cu.UserId == userId)
                .Include(cu => cu.Chat)
                .ToListAsync();

            var chats = publicChats.Union(userChats.Select(app => app.Chat));

            return chats;
        }

        public async static Task<bool> CreateMessage(string message)
        {
            var user = await User.GetUserBySession();
            if (user == null)
                return false;
            if (!await Security.HasAccessToChat(User.SelectedChat))
            {
                MessageBox.Show("You are not authorized to this action!");
                return false;
            }

            var newMessage = new ChatMessageEntity()
            {
                ChatId = User.SelectedChat.Id,
                UserId = user.Id,
                Text = message,
                AddedAt = Main.GetCurrentUnixTimestamp(),
            };
            if (!ModelState.IsValid<ChatMessageEntity>(newMessage))
                return ModelState.ShowErrorMessage();

            await Main.dbContext.ChatMessages.AddAsync(newMessage);
            await Main.dbContext.SaveChangesAsync();
            return true;
        }

        private async static Task<bool> ValidateName(string name)
        {
            var chats = await Main.dbContext.Chats.Where(c => c.Name == name).FirstOrDefaultAsync();
            if (chats != null)
            {
                _errorMessage = "A chat with this name already exists!";
                return false;
            }
            return true;
        }

        public async static Task<bool> CreateChat(string name, string password = null)
        {
            if ((password == null || password == "") && await User.GetUserRole() != UserRole.Admin)
            {
                MessageBox.Show("Field \"Chat password\" is required!");
                return false;
            }
            if (!await ValidateName(name))
            {
                MessageBox.Show(_errorMessage);
                return false;
            }
            if (!await AddChatToDatabase(name, password))
            {
                MessageBox.Show("Something went wrong ;<");
                return false;
            }
            MessageBox.Show("Chat has been successfully created!");
            return true;
        }

        private async static Task<bool> AddChatToDatabase(string name, string password = null)
        {
            try
            {
                var chat = new ChatEntity();
                chat.Name = name;

                if (password == null || password == "")
                {
                    chat.Type = ChatType.PublicChat;
                }
                else
                {
                    var salt = Security.GetSalt();
                    var hash = Security.GetHash(password, salt);

                    chat.Type = ChatType.PrivateChat;
                    chat.SecretKeyHash = hash;
                    chat.SecretKeySalt = salt;
                }

                await Main.dbContext.Chats.AddAsync(chat);

                await Main.dbContext.SaveChangesAsync();
                return true;
            }
            catch (Exception exc)
            {
                return false;
            }
        }
    }
}
