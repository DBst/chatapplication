﻿using ChatApplication.Entities;
using ChatApplication.Forms.Chats;
using Hanssens.Net;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO.IsolatedStorage;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Windows.Storage;
using static System.Net.Mime.MediaTypeNames;
using ChatApplication.Models.Dto;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;
using System.Windows.Markup;

namespace ChatApplication.Models
{
    internal class User
    {
        public static SessionDto Session;
        public static Chat SelectedChat;

        public static MdiParent Parent;

        public static string LastUsername;

        public static bool IsLogged()
        {
            return Session != null;
        }

        public async static Task<bool> SignIn(string username, string password, bool rememberMe)
        {
            var user = await Main.dbContext.Users
                .Where(c => c.Username == username)
                .FirstOrDefaultAsync();

            if (user == null || !VerifyPassword(password, user))
            {
                MessageBox.Show($"Incorrect username or password!");
                return false;
            }
            if (user.Status == UserStatus.Banned)
            {
                MessageBox.Show("Your account has been blocked!");
                return false;
            }

            Session = await CreateSessionData(user);

            if (rememberMe)
                SetAppCache(Session, username);

            return true;
        }
        public static void LogoutUser(string message)
        {
            Session = null;
            if (message != null)
                MessageBox.Show(message, message, MessageBoxButtons.OK, MessageBoxIcon.Error);

            var loginForm = new Login();
            loginForm.ShowDialog();
        }

        public async static Task<UserDto> GetUserBySession()
        {
            var data = await Main.dbContext.UserSessions
                .Where(us => us.Id == Session.SessionId)
                .Include(us => us.User).FirstOrDefaultAsync();

            if (data == null)
            {
                LogoutUser("User session is invalid!");
                RemoveAppCache();
                return null;
            }
            if (Main.GetCurrentUnixTimestamp() > data.ExpiredAt)
            {
                LogoutUser("Your login session has expired.");
                RemoveAppCache();
                return null;
            }
            if (data.User.Status == UserStatus.Banned)
            {
                LogoutUser("Your account has been blocked!");
                RemoveAppCache();
                return null;
            }

            var user = new UserDto()
            {
                Id = data.User.Id,
                Username = data.User.Username,
                Email = data.User.Email,
                Role = data.User.Role,
            };

            return user;
        }

        private static bool VerifyPassword(string givenPassword, UserEntity user)
        {
            var passwordHash = Security.GetHash(givenPassword, user.PasswordSalt);
            return Security.VerifyPassword(passwordHash, user.PasswordHash);
        }

        private static void SetAppCache(SessionDto session, string username)
        {
            var cache = new AppCache()
            {
                Session = session,
                Username = username,
            };
            var json = JsonConvert.SerializeObject(cache);
            File.WriteAllText("appCache.json", json);
        }

        public static AppCache GetAppCache()
        {
            try
            {
                var json = File.ReadAllText("appCache.json");
                var appCache = JsonConvert.DeserializeObject<AppCache>(json);

                return appCache;
            }
            catch (FileNotFoundException e)
            {
                return null;
            }
        }

        public static void RemoveAppCache()
        {
            try
            {
                File.Delete("appCache.json");
            }
            catch (FileNotFoundException e)
            {
            }
        }

        public async static Task LogInByCache(AppCache cache)
        {
            LastUsername = cache.Username;
            Session = cache.Session;
            await GetUserBySession();
        }

        private async static Task<SessionDto> CreateSessionData(UserEntity user)
        {
            UserSessionsEntity session = new UserSessionsEntity()
            {
                UserId = user.Id,
                CreatedAt = Main.GetCurrentUnixTimestamp(),
                ExpiredAt = Main.GetCurrentUnixTimestamp() + 60 * 60 // +1 hour
                //ExpiredAt = Main.GetCurrentUnixTimestamp() + 10 // +10 seconds
            };

            await Main.dbContext.UserSessions.AddAsync(session);
            await Main.dbContext.SaveChangesAsync();

            var createdSession = new SessionDto()
            {
                SessionId = session.Id,
                ExpiredAt = session.ExpiredAt,
                Data = new UserDto()
                {
                    Username = user.Username,
                    Email = user.Email,
                    RecentlyOnlineAt = user.RecentlyOnlineAt,
                    Role = user.Role,
                },
            };

            return createdSession;
        }

        public async static Task<bool> UpdateRecentlyOnlineAt()
        {
            var dbContext = new ChatDbContext();
            var session = await dbContext.UserSessions
                .Where(us => us.Id == Session.SessionId)
                .Include(us => us.User)
                .FirstOrDefaultAsync();

            if (session == null)
                return false;

            var user = session.User;

            user.RecentlyOnlineAt = Main.GetCurrentUnixTimestamp();
            await dbContext.SaveChangesAsync();

            return true;
        }

        public async static Task<bool> JoinChat(string chatName, string chatPassword)
        {
            var chat = await Main.dbContext.Chats
                .FirstOrDefaultAsync(c => c.Name == chatName && c.Type == ChatType.PrivateChat);
            if (chat == null)
            {
                MessageBox.Show("Cannot find chat with this name!");
                return false;
            }

            var hash = Security.GetHash(chatPassword, chat.SecretKeySalt);

            if (!Security.VerifyPassword(hash, chat.SecretKeyHash))
            {
                MessageBox.Show("Password is not correct!");
                return false;
            }

            var user = await GetUserBySession();
            var userId = user.Id;

            var chatAccess = new UserAccessEntity()
            {
                ChatId = chat.Id,
                UserId = userId,
            };

            await Main.dbContext.AddAsync(chatAccess);
            await Main.dbContext.SaveChangesAsync();

            return true;
        }

        public async static Task<bool> CreateUser(string username, string email, string password, string passwordRepeat)
        {
            byte[] salt = Security.GetSalt();
            byte[] passwordHash = Security.GetHash(password, salt);
            UserEntity user = new UserEntity()
            {
                Username = username,
                Email = email,
                PasswordHash = passwordHash,
                PasswordSalt = salt,
                Status = UserStatus.Default,
                Role = UserRole.Admin,
            };

            if (!ModelState.IsValid<UserEntity>(user))
                return ModelState.ShowErrorMessage();
            if (!PasswordIsValid(password, passwordRepeat))
                return false;
            if (!await IsUserDataAvailable(username, email))
                return false;

            await Main.dbContext.Users.AddAsync(user);
            await Main.dbContext.SaveChangesAsync();
            return true;
        }

        private static bool PasswordIsValid(string password, string passwordRepeat)
        {
            var hasNumber = new Regex(@"[0-9]+");
            var hasUpperChar = new Regex(@"[A-Z]+");
            var hasMinimumLength = new Regex(@".{8,}");

            if (password != passwordRepeat)
                return ModelState.ShowErrorMessage("Passwords are not the same!");
            else if (!hasNumber.IsMatch(password))
                return ModelState.ShowErrorMessage("Password should contain at least one number!");
            else if (!hasUpperChar.IsMatch(password))
                return ModelState.ShowErrorMessage("Password should contain at least one upper case letter!");
            else if (!hasMinimumLength.IsMatch(password))
                return ModelState.ShowErrorMessage("Password should not be less than or greater than 8 characters!");

            return true;
        }

        private async static Task<bool> IsUserDataAvailable(string username, string email)
        {
            var user = await Main.dbContext.Users
                .FirstOrDefaultAsync(u => u.Username == username || u.Email == email);

            if (user != null)
            {
                if (user.Username == username)
                    MessageBox.Show("User with this username already exists!");
                if (user.Email == email)
                    MessageBox.Show("User with this email already exists!");
                return false;
            }

            return true;
        }

        public static async Task<UserRole> GetUserRole(UserDto user = null)
        {
            if(user == null)
                user = await GetUserBySession();

            return user.Role;
        }

        public static async Task<bool> ChangeUserStatus(Guid userId, UserStatus status)
        {
            var user = await GetUserBySession();

            if (user.Id == userId)
                return false;
            if (user.Role != UserRole.Admin)
                return false;

            var selectedUser = await Main.dbContext.Users.FirstOrDefaultAsync(u => u.Id == userId);

            if (selectedUser.Status == status)
                return false;

            selectedUser.Status = status;
            await Main.dbContext.SaveChangesAsync();

            return true;
        }

    }
}
