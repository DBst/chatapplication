﻿using ChatApplication.Forms.Chats;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.EntityFrameworkCore;
using System;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace ChatApplication.Models
{
    internal class Security
    {
        private const int _saltSize = 16;
        private const int _iterations = 100000;

        public static byte[] GetHash(string password, byte[] salt)
        {
            byte[] hashed = KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA256,
                iterationCount: _iterations,
                numBytesRequested: 256 / 8
            );

            return hashed;
        }

        public static byte[] GetSalt()
        {
            byte[] salt = new byte[_saltSize];
            using (var rngCsp = new RNGCryptoServiceProvider())
            {
                rngCsp.GetNonZeroBytes(salt);
            }

            return salt;
        }

        public static bool VerifyPassword(byte[] password, byte[] userPassword)
        {
            if (password.Length != userPassword.Length)
                return false;

            for (int i = 0; i < password.Length; i++)
                if (password[i] != userPassword[i])
                    return false;

            return true;
        }

        public static async Task<bool> HasAccessToChat(Chat chat)
        {
            if (chat.Type == ChatType.PublicChat)
                return true;

            var user = await User.GetUserBySession();
            var userId = user.Id;

            var userAccess = await Main.dbContext.UserAccesses
                .Where(cu => cu.UserId == userId && cu.ChatId == chat.Id)
                .CountAsync();

            return userAccess > 0;
        }

    }
}
