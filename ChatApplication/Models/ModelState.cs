﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatApplication.Models
{
    public static class ModelState
    {
        public static string ErrorMessage = string.Empty;

        public static bool IsValid<T>(T model)
        {
            var validationContext = new ValidationContext(model, null, null);
            var results = new List<ValidationResult>();

            if (Validator.TryValidateObject(model, validationContext, results, true))
                return true;

            ErrorMessage = results[0].ErrorMessage;
            return false;

        }

        public static bool ShowErrorMessage(string message = null)
        {
            if(message == null)
                message = ErrorMessage;

            MessageBox.Show(message);
            return false;
        }
    }
}
