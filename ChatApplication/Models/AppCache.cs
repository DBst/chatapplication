﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication.Models
{
    internal class AppCache
    {
        public SessionDto Session { get; set; }
        public string Username { get; set; }
    }
}
