﻿using ChatApplication.Entities;
using ChatApplication.Models.Dto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChatApplication.Models
{
    public class SessionDto
    {
        public Guid SessionId { get; set; }
        public int ExpiredAt { get; set; }
        public UserDto Data { get; set; }
    }
}
