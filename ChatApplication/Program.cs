﻿using ChatApplication.Forms.Chats;
using ChatApplication.Models;
using FluentAssertions.Common;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Runtime.Remoting.Contexts;
using System.Windows.Forms;

namespace ChatApplication
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            new Main();
            Application.Run(new MdiParent());
        }
    }
}
