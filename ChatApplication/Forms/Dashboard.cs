﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatApplication.Entities;
using ChatApplication.Models;

namespace ChatApplication.Forms.Chats
{
    public partial class Dashboard : Form
    {
        public Dashboard()
        {
            InitializeComponent();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            if (User.Session.Data.Role == UserRole.Admin)
                CreateChatPasswordLabel.Text = "Chat password (optional)";
        }

        private async void SendForm_CreateChat()
        {
            var chatName = CreateChatNameInput.Text;
            var chatPassword = CreateChatPasswordInput.Text;

            if (await Chat.CreateChat(chatName, chatPassword))
            {
                CreateChatNameInput.Text = "";
                CreateChatPasswordInput.Text = "";
                await Chat.RefreshChats();
            }
        }

        private void CreateChat_ButtonClick(object sender, EventArgs e)
        {
            SendForm_CreateChat();
        }

        private void CreateChat_PressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
                return;

            SendForm_CreateChat();
        }

        private async void SendForm_JoinChat()
        {
            var chatName = JoinChatNameInput.Text;
            var password = JoinChatPasswordInput.Text;

            if (await User.JoinChat(chatName, password))
            {
                JoinChatNameInput.Text = "";
                JoinChatPasswordInput.Text = "";
                await Chat.RefreshChats();
            }
        }

        private void JoinChat_ButtonClick(object sender, EventArgs e)
        {
            SendForm_JoinChat();
        }

        private void JoinChat_PressEnter(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
                return;

            SendForm_JoinChat();
        }

        private void Logout_Click(object sender, EventArgs e)
        {
            User.RemoveAppCache();
            User.LogoutUser(null);
        }
    }
}
