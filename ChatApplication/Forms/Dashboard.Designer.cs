﻿namespace ChatApplication.Forms.Chats
{
    partial class Dashboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DashboardInfo = new System.Windows.Forms.Label();
            this.CreateChatNameInput = new System.Windows.Forms.TextBox();
            this.CreateChatNameLabel = new System.Windows.Forms.Label();
            this.CreateChatButton = new System.Windows.Forms.Button();
            this.CreateChatPasswordInput = new System.Windows.Forms.TextBox();
            this.CreateChatPasswordLabel = new System.Windows.Forms.Label();
            this.CreatingChatLabel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.JoinChatPasswordLabel = new System.Windows.Forms.Label();
            this.JoinChatPasswordInput = new System.Windows.Forms.TextBox();
            this.JoinChatButton = new System.Windows.Forms.Button();
            this.JoinChatNameLabel = new System.Windows.Forms.Label();
            this.JoinChatNameInput = new System.Windows.Forms.TextBox();
            this.Logout = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // DashboardInfo
            // 
            this.DashboardInfo.AutoSize = true;
            this.DashboardInfo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.DashboardInfo.ForeColor = System.Drawing.Color.White;
            this.DashboardInfo.Location = new System.Drawing.Point(12, 9);
            this.DashboardInfo.Name = "DashboardInfo";
            this.DashboardInfo.Size = new System.Drawing.Size(83, 25);
            this.DashboardInfo.TabIndex = 0;
            this.DashboardInfo.Text = "Actions";
            // 
            // CreateChatNameInput
            // 
            this.CreateChatNameInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.CreateChatNameInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CreateChatNameInput.ForeColor = System.Drawing.Color.White;
            this.CreateChatNameInput.Location = new System.Drawing.Point(149, 209);
            this.CreateChatNameInput.Name = "CreateChatNameInput";
            this.CreateChatNameInput.Size = new System.Drawing.Size(195, 20);
            this.CreateChatNameInput.TabIndex = 4;
            this.CreateChatNameInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CreateChat_PressEnter);
            // 
            // CreateChatNameLabel
            // 
            this.CreateChatNameLabel.AutoSize = true;
            this.CreateChatNameLabel.ForeColor = System.Drawing.Color.White;
            this.CreateChatNameLabel.Location = new System.Drawing.Point(20, 212);
            this.CreateChatNameLabel.Name = "CreateChatNameLabel";
            this.CreateChatNameLabel.Size = new System.Drawing.Size(62, 13);
            this.CreateChatNameLabel.TabIndex = 2;
            this.CreateChatNameLabel.Text = "Chat name*";
            // 
            // CreateChatButton
            // 
            this.CreateChatButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.CreateChatButton.FlatAppearance.BorderSize = 0;
            this.CreateChatButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CreateChatButton.ForeColor = System.Drawing.Color.White;
            this.CreateChatButton.Location = new System.Drawing.Point(149, 261);
            this.CreateChatButton.Name = "CreateChatButton";
            this.CreateChatButton.Size = new System.Drawing.Size(195, 28);
            this.CreateChatButton.TabIndex = 6;
            this.CreateChatButton.Text = "Create";
            this.CreateChatButton.UseVisualStyleBackColor = false;
            this.CreateChatButton.Click += new System.EventHandler(this.CreateChat_ButtonClick);
            // 
            // CreateChatPasswordInput
            // 
            this.CreateChatPasswordInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.CreateChatPasswordInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.CreateChatPasswordInput.ForeColor = System.Drawing.Color.White;
            this.CreateChatPasswordInput.Location = new System.Drawing.Point(149, 235);
            this.CreateChatPasswordInput.Name = "CreateChatPasswordInput";
            this.CreateChatPasswordInput.Size = new System.Drawing.Size(195, 20);
            this.CreateChatPasswordInput.TabIndex = 5;
            this.CreateChatPasswordInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CreateChat_PressEnter);
            // 
            // CreateChatPasswordLabel
            // 
            this.CreateChatPasswordLabel.AutoSize = true;
            this.CreateChatPasswordLabel.ForeColor = System.Drawing.Color.White;
            this.CreateChatPasswordLabel.Location = new System.Drawing.Point(20, 237);
            this.CreateChatPasswordLabel.Name = "CreateChatPasswordLabel";
            this.CreateChatPasswordLabel.Size = new System.Drawing.Size(81, 13);
            this.CreateChatPasswordLabel.TabIndex = 5;
            this.CreateChatPasswordLabel.Text = "Chat password*";
            // 
            // CreatingChatLabel
            // 
            this.CreatingChatLabel.AutoSize = true;
            this.CreatingChatLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CreatingChatLabel.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.CreatingChatLabel.Location = new System.Drawing.Point(13, 179);
            this.CreatingChatLabel.Name = "CreatingChatLabel";
            this.CreatingChatLabel.Size = new System.Drawing.Size(138, 18);
            this.CreatingChatLabel.TabIndex = 6;
            this.CreatingChatLabel.Text = "Creating a new chat";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(13, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(127, 18);
            this.label1.TabIndex = 12;
            this.label1.Text = "Join a private chat";
            // 
            // JoinChatPasswordLabel
            // 
            this.JoinChatPasswordLabel.AutoSize = true;
            this.JoinChatPasswordLabel.ForeColor = System.Drawing.Color.White;
            this.JoinChatPasswordLabel.Location = new System.Drawing.Point(20, 112);
            this.JoinChatPasswordLabel.Name = "JoinChatPasswordLabel";
            this.JoinChatPasswordLabel.Size = new System.Drawing.Size(81, 13);
            this.JoinChatPasswordLabel.TabIndex = 11;
            this.JoinChatPasswordLabel.Text = "Chat password*";
            // 
            // JoinChatPasswordInput
            // 
            this.JoinChatPasswordInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.JoinChatPasswordInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JoinChatPasswordInput.ForeColor = System.Drawing.Color.White;
            this.JoinChatPasswordInput.Location = new System.Drawing.Point(149, 110);
            this.JoinChatPasswordInput.Name = "JoinChatPasswordInput";
            this.JoinChatPasswordInput.Size = new System.Drawing.Size(195, 20);
            this.JoinChatPasswordInput.TabIndex = 2;
            this.JoinChatPasswordInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.JoinChat_PressEnter);
            // 
            // JoinChatButton
            // 
            this.JoinChatButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.JoinChatButton.FlatAppearance.BorderSize = 0;
            this.JoinChatButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.JoinChatButton.ForeColor = System.Drawing.Color.White;
            this.JoinChatButton.Location = new System.Drawing.Point(149, 136);
            this.JoinChatButton.Name = "JoinChatButton";
            this.JoinChatButton.Size = new System.Drawing.Size(195, 28);
            this.JoinChatButton.TabIndex = 3;
            this.JoinChatButton.Text = "Join";
            this.JoinChatButton.UseVisualStyleBackColor = false;
            this.JoinChatButton.Click += new System.EventHandler(this.JoinChat_ButtonClick);
            // 
            // JoinChatNameLabel
            // 
            this.JoinChatNameLabel.AutoSize = true;
            this.JoinChatNameLabel.ForeColor = System.Drawing.Color.White;
            this.JoinChatNameLabel.Location = new System.Drawing.Point(20, 87);
            this.JoinChatNameLabel.Name = "JoinChatNameLabel";
            this.JoinChatNameLabel.Size = new System.Drawing.Size(62, 13);
            this.JoinChatNameLabel.TabIndex = 8;
            this.JoinChatNameLabel.Text = "Chat name*";
            // 
            // JoinChatNameInput
            // 
            this.JoinChatNameInput.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.JoinChatNameInput.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.JoinChatNameInput.ForeColor = System.Drawing.Color.White;
            this.JoinChatNameInput.Location = new System.Drawing.Point(149, 84);
            this.JoinChatNameInput.Name = "JoinChatNameInput";
            this.JoinChatNameInput.Size = new System.Drawing.Size(195, 20);
            this.JoinChatNameInput.TabIndex = 1;
            this.JoinChatNameInput.KeyUp += new System.Windows.Forms.KeyEventHandler(this.JoinChat_PressEnter);
            // 
            // Logout
            // 
            this.Logout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Logout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.Logout.FlatAppearance.BorderSize = 0;
            this.Logout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Logout.ForeColor = System.Drawing.Color.White;
            this.Logout.Location = new System.Drawing.Point(702, 10);
            this.Logout.Name = "Logout";
            this.Logout.Size = new System.Drawing.Size(86, 24);
            this.Logout.TabIndex = 13;
            this.Logout.Text = "Logout";
            this.Logout.UseVisualStyleBackColor = false;
            this.Logout.Click += new System.EventHandler(this.Logout_Click);
            // 
            // Dashboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Logout);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.JoinChatPasswordLabel);
            this.Controls.Add(this.JoinChatPasswordInput);
            this.Controls.Add(this.JoinChatButton);
            this.Controls.Add(this.JoinChatNameLabel);
            this.Controls.Add(this.JoinChatNameInput);
            this.Controls.Add(this.CreatingChatLabel);
            this.Controls.Add(this.CreateChatPasswordLabel);
            this.Controls.Add(this.CreateChatPasswordInput);
            this.Controls.Add(this.CreateChatButton);
            this.Controls.Add(this.CreateChatNameLabel);
            this.Controls.Add(this.CreateChatNameInput);
            this.Controls.Add(this.DashboardInfo);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Dashboard";
            this.Text = "Dashboard";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label DashboardInfo;
        private System.Windows.Forms.TextBox CreateChatNameInput;
        private System.Windows.Forms.Label CreateChatNameLabel;
        private System.Windows.Forms.Button CreateChatButton;
        private System.Windows.Forms.TextBox CreateChatPasswordInput;
        private System.Windows.Forms.Label CreateChatPasswordLabel;
        private System.Windows.Forms.Label CreatingChatLabel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label JoinChatPasswordLabel;
        private System.Windows.Forms.TextBox JoinChatPasswordInput;
        private System.Windows.Forms.Button JoinChatButton;
        private System.Windows.Forms.Label JoinChatNameLabel;
        private System.Windows.Forms.TextBox JoinChatNameInput;
        private System.Windows.Forms.Button Logout;
    }
}