﻿using ChatApplication.Models;
using ChatApplication.Entities;
using Microsoft.AspNetCore.Http;
using System;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Web.SessionState;
using System.Windows.Forms;
using System.Threading.Tasks;

namespace ChatApplication
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
            LoginField.Text = User.LastUsername;
        }

        private void OpenRegistrationForm(object sender, EventArgs e)
        {
            var registrationForm = new Registration();
            registrationForm.ShowDialog();
        }

        private void CloseForm(object sender, FormClosingEventArgs e)
        {
            if(!User.IsLogged())
                Environment.Exit(0);
        }

        private async void OnClickSend(object sender, EventArgs e)
        {
            await this.SendForm();
        }

        private async void OnEnterPassword(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Return)
                await this.SendForm();
        }

        private async Task SendForm()
        {
            var username = LoginField.Text;
            var password = PasswordField.Text;
            var rememberMe = RememberMe.Checked;

            if (await User.SignIn(username, password, rememberMe))
            {
                this.Close();
                await User.Parent.SuccessfulLogIn();
            }
        }
    }
}
