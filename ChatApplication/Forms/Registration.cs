﻿using ChatApplication.Entities;
using ChatApplication.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatApplication
{
    public partial class Registration : Form
    {
        private string _errorMessage;

        public Registration()
        {
            InitializeComponent();
        }

        private void Registration_Load(object sender, EventArgs e)
        {

        }

        //register button
        private async void RegisterUser_Click(object sender, EventArgs e)
        {
            await RegisterUser();
        }

        private async void RegisterUser_Send(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
                return;

            await RegisterUser();
        }

        public async Task RegisterUser()
        {
            var username = UsernameInput.Text;
            var email = EmailInput.Text;
            var password = PasswordInput.Text;
            var repeatPassword = PasswordRepeatInput.Text;

            if (!await User.CreateUser(username, email, password, repeatPassword))
                return;

            MessageBox.Show("Your account has been successfully created!");
            this.Hide();
        }

    }
}
