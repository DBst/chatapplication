﻿using ChatApplication.Models;
using ChatApplication.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.EntityFrameworkCore;

namespace ChatApplication.Forms.Chats
{
    public partial class MdiParent : Form
    {
        public MdiParent()
        {
            InitializeComponent();
        }

        private async void Parent_Load(object sender, EventArgs e)
        {
            User.Parent = this;
            var cache = User.GetAppCache();

            if (cache != null)
                await User.LogInByCache(cache);

            if (!User.IsLogged())
            {
                var loginForm = new Login();
                loginForm.ShowDialog();
            }
            else
            {
                await User.Parent.SuccessfulLogIn();
            }
        }

        public async Task SuccessfulLogIn()
        {
            await Chat.RefreshChats();
            var sender_ = ChatItem.GetChatItem("Dashboard", ChatType.Dashboard, Guid.Empty);
            Models.Chat.OpenView(sender_);
        }

        public ListBox GetOnlineUsersListBox()
        {
            ListBox UsersOnline = (ListBox)Chat.GetFieldByName(this.Controls, "UsersOnline");
            return UsersOnline;
        }

        public ListBox GetOfflineUsersListBox()
        {
            ListBox UsersOffline = (ListBox)Chat.GetFieldByName(this.Controls, "UsersOffline");
            return UsersOffline;
        }

        private void OnResizeWindow(object sender, EventArgs e)
        {
            Chat.ChangeMessageFieldSize(this.ClientSize.Width - 420);
        }
    }
}
