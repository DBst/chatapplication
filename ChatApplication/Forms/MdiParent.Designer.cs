﻿using System;
using System.Runtime.InteropServices;
namespace ChatApplication.Forms.Chats
{
    partial class MdiParent
    {
        [DllImport("dwmapi.dll", PreserveSig = true)]
        public static extern int DwmSetWindowAttribute(IntPtr hwnd, int attr, ref bool attrValue, int attrSize);
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.ChatList = new System.Windows.Forms.FlowLayoutPanel();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.test2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tes3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UsersListPanel = new System.Windows.Forms.Panel();
            this.UsersOffline = new System.Windows.Forms.ListBox();
            this.UsersOfflineLabel = new System.Windows.Forms.Label();
            this.UsersOnline = new System.Windows.Forms.ListBox();
            this.UsersOnlineLabel = new System.Windows.Forms.Label();
            this.UsersListPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(32, 19);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(32, 19);
            // 
            // ChatList
            // 
            this.ChatList.AutoScroll = true;
            this.ChatList.Dock = System.Windows.Forms.DockStyle.Left;
            this.ChatList.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.ChatList.Location = new System.Drawing.Point(0, 0);
            this.ChatList.Name = "ChatList";
            this.ChatList.Padding = new System.Windows.Forms.Padding(3);
            this.ChatList.Size = new System.Drawing.Size(200, 476);
            this.ChatList.TabIndex = 3;
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // test2ToolStripMenuItem
            // 
            this.test2ToolStripMenuItem.Name = "test2ToolStripMenuItem";
            this.test2ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // tes3ToolStripMenuItem
            // 
            this.tes3ToolStripMenuItem.Name = "tes3ToolStripMenuItem";
            this.tes3ToolStripMenuItem.Size = new System.Drawing.Size(32, 19);
            // 
            // UsersListPanel
            // 
            this.UsersListPanel.Controls.Add(this.UsersOffline);
            this.UsersListPanel.Controls.Add(this.UsersOfflineLabel);
            this.UsersListPanel.Controls.Add(this.UsersOnline);
            this.UsersListPanel.Controls.Add(this.UsersOnlineLabel);
            this.UsersListPanel.Dock = System.Windows.Forms.DockStyle.Right;
            this.UsersListPanel.Location = new System.Drawing.Point(792, 0);
            this.UsersListPanel.Name = "UsersListPanel";
            this.UsersListPanel.Size = new System.Drawing.Size(154, 476);
            this.UsersListPanel.TabIndex = 4;
            // 
            // UsersOffline
            // 
            this.UsersOffline.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersOffline.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.UsersOffline.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UsersOffline.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.UsersOffline.FormattingEnabled = true;
            this.UsersOffline.Location = new System.Drawing.Point(3, 261);
            this.UsersOffline.Name = "UsersOffline";
            this.UsersOffline.Size = new System.Drawing.Size(148, 208);
            this.UsersOffline.TabIndex = 3;
            // 
            // UsersOfflineLabel
            // 
            this.UsersOfflineLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersOfflineLabel.AutoSize = true;
            this.UsersOfflineLabel.ForeColor = System.Drawing.SystemColors.ButtonFace;
            this.UsersOfflineLabel.Location = new System.Drawing.Point(3, 245);
            this.UsersOfflineLabel.Name = "UsersOfflineLabel";
            this.UsersOfflineLabel.Size = new System.Drawing.Size(68, 13);
            this.UsersOfflineLabel.TabIndex = 2;
            this.UsersOfflineLabel.Text = "Users offline:";
            // 
            // UsersOnline
            // 
            this.UsersOnline.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersOnline.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.UsersOnline.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UsersOnline.ForeColor = System.Drawing.SystemColors.MenuBar;
            this.UsersOnline.FormattingEnabled = true;
            this.UsersOnline.Location = new System.Drawing.Point(3, 21);
            this.UsersOnline.Name = "UsersOnline";
            this.UsersOnline.Size = new System.Drawing.Size(148, 221);
            this.UsersOnline.TabIndex = 1;
            // 
            // UsersOnlineLabel
            // 
            this.UsersOnlineLabel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.UsersOnlineLabel.AutoSize = true;
            this.UsersOnlineLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsersOnlineLabel.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.UsersOnlineLabel.Location = new System.Drawing.Point(3, 5);
            this.UsersOnlineLabel.Name = "UsersOnlineLabel";
            this.UsersOnlineLabel.Size = new System.Drawing.Size(68, 13);
            this.UsersOnlineLabel.TabIndex = 0;
            this.UsersOnlineLabel.Text = "Users online:";
            // 
            // MdiParent
            // 
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(37)))), ((int)(((byte)(37)))), ((int)(((byte)(38)))));
            this.ClientSize = new System.Drawing.Size(946, 476);
            this.Controls.Add(this.ChatList);
            this.Controls.Add(this.UsersListPanel);
            this.IsMdiContainer = true;
            this.Name = "MdiParent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Chat Application";
            this.Load += new System.EventHandler(this.Parent_Load);
            this.ResizeEnd += new System.EventHandler(this.OnResizeWindow);
            this.UsersListPanel.ResumeLayout(false);
            this.UsersListPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem test2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tes3ToolStripMenuItem;
        private System.Windows.Forms.FlowLayoutPanel ChatList;
        private System.Windows.Forms.Panel UsersListPanel;
        private System.Windows.Forms.Label UsersOnlineLabel;
        private System.Windows.Forms.ListBox UsersOnline;
        private System.Windows.Forms.ListBox UsersOffline;
        private System.Windows.Forms.Label UsersOfflineLabel;
    }
}