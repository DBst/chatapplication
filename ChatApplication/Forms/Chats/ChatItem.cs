﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChatApplication.Forms.Chats;
using ChatApplication.Models;

namespace ChatApplication.Forms.Chats
{
    public enum ChatType
    {
        Undefined,
        Dashboard,
        PublicChat,
        PrivateChat
    }
    public partial class ChatItem : UserControl
    {
        private string _name;
        public ChatType Type;
        public Guid Id;

        public ChatItem()
        {
            InitializeComponent();
        }

        [Category("Custom props")]
        public new string Name
        {
            get { return _name; }
            set { _name = value; chatName.Text = value; }
        }

        public static ChatItem GetChatItem(string name, ChatType type, Guid id)
        {
            var item = new ChatItem();
            item.Name = name;
            item.Type = type;
            item.Id = id;
            item.MouseEnter += OnMouseHover;
            item.MouseLeave += OnMouseOut;
            item.Click += Models.Chat.OpenView;
            return item;
        }

        private static void OnMouseHover(object sender, EventArgs e)
        {
            ChatItem item = sender as ChatItem;
            item.BackColor = Color.Silver;
        }

        private static void OnMouseOut(object sender, EventArgs e)
        {
            ChatItem item = sender as ChatItem;
            item.BackColor = Color.FromArgb(51, 51, 51);
        }
    }
}
