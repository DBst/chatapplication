﻿namespace ChatApplication.Forms.Chats
{
    partial class ChatItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chatName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // chatName
            // 
            this.chatName.AutoSize = true;
            this.chatName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.chatName.ForeColor = System.Drawing.Color.White;
            this.chatName.Location = new System.Drawing.Point(14, 11);
            this.chatName.Name = "chatName";
            this.chatName.Size = new System.Drawing.Size(46, 16);
            this.chatName.TabIndex = 0;
            this.chatName.Text = "Chat1";
            // 
            // ChatItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.Controls.Add(this.chatName);
            this.Name = "ChatItem";
            this.Size = new System.Drawing.Size(188, 39);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label chatName;
    }
}
