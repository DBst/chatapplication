﻿namespace ChatApplication.Forms.Chats
{
    partial class MessageItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameField = new System.Windows.Forms.Label();
            this.MessageField = new System.Windows.Forms.Label();
            this.TimeField = new System.Windows.Forms.Label();
            this.ActionRemoveMessage = new System.Windows.Forms.Label();
            this.ActionBan = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UsernameField
            // 
            this.UsernameField.AutoSize = true;
            this.UsernameField.Dock = System.Windows.Forms.DockStyle.Left;
            this.UsernameField.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.UsernameField.Location = new System.Drawing.Point(5, 5);
            this.UsernameField.Name = "UsernameField";
            this.UsernameField.Size = new System.Drawing.Size(63, 13);
            this.UsernameField.TabIndex = 0;
            this.UsernameField.Text = "Username";
            // 
            // MessageField
            // 
            this.MessageField.AutoSize = true;
            this.MessageField.Location = new System.Drawing.Point(12, 26);
            this.MessageField.Margin = new System.Windows.Forms.Padding(0, 0, 0, 5);
            this.MessageField.MaximumSize = new System.Drawing.Size(525, 0);
            this.MessageField.Name = "MessageField";
            this.MessageField.Size = new System.Drawing.Size(50, 13);
            this.MessageField.TabIndex = 1;
            this.MessageField.Text = "Message";
            // 
            // TimeField
            // 
            this.TimeField.AutoSize = true;
            this.TimeField.Dock = System.Windows.Forms.DockStyle.Left;
            this.TimeField.Location = new System.Drawing.Point(68, 5);
            this.TimeField.Name = "TimeField";
            this.TimeField.Size = new System.Drawing.Size(30, 13);
            this.TimeField.TabIndex = 2;
            this.TimeField.Text = "Time";
            // 
            // ActionRemoveMessage
            // 
            this.ActionRemoveMessage.AutoSize = true;
            this.ActionRemoveMessage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ActionRemoveMessage.Dock = System.Windows.Forms.DockStyle.Right;
            this.ActionRemoveMessage.Location = new System.Drawing.Point(622, 5);
            this.ActionRemoveMessage.Name = "ActionRemoveMessage";
            this.ActionRemoveMessage.Size = new System.Drawing.Size(93, 13);
            this.ActionRemoveMessage.TabIndex = 3;
            this.ActionRemoveMessage.Text = "Remove Message";
            this.ActionRemoveMessage.Visible = false;
            this.ActionRemoveMessage.Click += new System.EventHandler(this.ActionRemoveMessage_Click);
            // 
            // ActionBan
            // 
            this.ActionBan.AutoSize = true;
            this.ActionBan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ActionBan.Dock = System.Windows.Forms.DockStyle.Right;
            this.ActionBan.Location = new System.Drawing.Point(596, 5);
            this.ActionBan.Name = "ActionBan";
            this.ActionBan.Size = new System.Drawing.Size(26, 13);
            this.ActionBan.TabIndex = 4;
            this.ActionBan.Text = "Ban";
            this.ActionBan.Visible = false;
            this.ActionBan.Click += new System.EventHandler(this.ActionBan_Click);
            // 
            // MessageItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(51)))), ((int)(((byte)(51)))));
            this.Controls.Add(this.ActionBan);
            this.Controls.Add(this.ActionRemoveMessage);
            this.Controls.Add(this.TimeField);
            this.Controls.Add(this.MessageField);
            this.Controls.Add(this.UsernameField);
            this.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Name = "MessageItem";
            this.Padding = new System.Windows.Forms.Padding(5);
            this.Size = new System.Drawing.Size(720, 77);
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UsernameField;
        private System.Windows.Forms.Label MessageField;
        private System.Windows.Forms.Label TimeField;
        private System.Windows.Forms.Label ActionRemoveMessage;
        private System.Windows.Forms.Label ActionBan;
    }
}
