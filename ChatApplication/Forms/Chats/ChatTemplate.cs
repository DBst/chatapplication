﻿using ChatApplication.Entities;
using ChatApplication.Models;
using ChatApplication.Models.Dto;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;
using System.Windows.Forms;
using static System.Net.Mime.MediaTypeNames;
using ListBox = System.Windows.Forms.ListBox;
using Panel = System.Windows.Forms.Panel;

namespace ChatApplication.Forms.Chats
{
    public partial class ChatTemplate : Form
    {
        public Chat Chat { get; set; }
        public bool RunInterval = true;

        public ChatTemplate(Chat chat)
        {
            Chat = chat;
            InitializeComponent();
            LoadChatMessages();
            InitUsersListFeature();
        }

        public void LoadChatMessages()
        {
            Chat.LoadMessagesThread = Task.Run(async () =>
            {
                var dbContext = new ChatDbContext();
                while (RunInterval)
                {
                    await UpdateMessages(dbContext);
                    Chat.LoadMessagesThread.AsTask().Wait(1000);
                }
            });
        }

        public async Task UpdateMessages(ChatDbContext dbContext)
        {
            var messages = await Chat.GetMessages(dbContext);

            var recentlyDeletedMessages = await Chat.GetRecentlyDeletedMessages(dbContext);

            foreach (var message in messages)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    AddMessageToChat(message);
                    this.MessagesBox.VerticalScroll.Value = this.MessagesBox.VerticalScroll.Maximum;
                    this.MessagesBox.VerticalScroll.Value = this.MessagesBox.VerticalScroll.Maximum;
                });
            }
            foreach (var message in recentlyDeletedMessages)
            {
                this.Invoke((MethodInvoker)delegate
                {
                    RemoveMessageFromChat(message);
                });
            }

        }

        private void RemoveMessageFromChat(ChatMessageEntity message)
        {
            foreach(var control in MessagesBox.Controls)
            {
                var item = control as MessageItem;
                if (item.GetId() == message.Id)
                {
                    item.HideMessage();
                }
            }
        }

        public void InitUsersListFeature()
        {
            ListBox UsersOnline = User.Parent.GetOnlineUsersListBox();
            ListBox UsersOffline = User.Parent.GetOfflineUsersListBox();

            if (UsersOnline == null || UsersOffline == null)
                return;

            Chat.OnlineUsersThread = Task.Run(async () =>
            {
                while (RunInterval)
                {
                    if (!await User.UpdateRecentlyOnlineAt())
                        return;

                    await UpdateUsersList(UsersOnline, UsersOffline);

                    Chat.OnlineUsersThread.AsTask().Wait(60000);
                }
            });
        }

        private async Task UpdateUsersList(ListBox UsersOnline, ListBox UsersOffline)
        {
            var time = Main.GetCurrentUnixTimestamp();
            var users = await Chat.GetUsersList();

            this.Invoke((MethodInvoker)delegate
            {
                UsersOnline.Items.Clear();
                UsersOffline.Items.Clear();
                foreach (UserDto user in users)
                {
                    if ((time - 60) < user.RecentlyOnlineAt)
                        UsersOnline.Items.Add(user.Username);
                    else
                        UsersOffline.Items.Add(user.Username);
                }
            });
        }

        public void AddMessageToChat(ChatMessageEntity message)
        {
            string time = Main.ConvertUnixToTime(message.AddedAt);

            string username = message.User.Username;
            string text = message.Text;

            var item = new MessageItem(message.Id, message.UserId)
            {
                Username = username,
                Message = text,
                Time = time,
            };

            MessagesBox.Controls.Add(item);
            if (message.DeletedAt != 0)
                item.HideMessage();
        }

        private async void ClickButton_SendMessage(object sender, EventArgs e)
        {
            await SendMessage(TextBox.Text);
        }

        private async Task SendMessage(string message)
        {
            if (await Chat.CreateMessage(message))
                TextBox.Text = string.Empty;
        }

        private async void PressEnter_SendMessage(object sender, KeyEventArgs e)
        {
            if (e.KeyCode != Keys.Return)
                return;

            await SendMessage(TextBox.Text);
        }
    }
}
