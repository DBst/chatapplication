﻿using ChatApplication.Entities;
using ChatApplication.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChatApplication.Forms.Chats
{
    public partial class MessageItem : UserControl
    {
        private readonly int _id;
        private readonly Guid _userId;
        private string _username;
        private string _message;
        private string _time;
        public bool IsHide = false;

        public MessageItem(int id, Guid userId)
        {
            _id = id;
            _userId = userId;
            InitializeComponent();
        }

        [Category("Custom props")]
        public new string Username
        {
            get { return _username; }
            set { _username = value; UsernameField.Text = value; }
        }

        [Category("Custom props")]
        public new string Message
        {
            get { return _message; }
            set { _message = value; MessageField.Text = value; }
        }

        [Category("Custom props")]
        public new string Time
        {
            get { return _time; }
            set { _time = value; TimeField.Text = "(" + value + ")"; }
        }

        private async void ActionRemoveMessage_Click(object sender, EventArgs e)
        {
            if (!await ChatMessage.RemoveMessage(_id))
                MessageBox.Show("Something went wrong ;<");
        }

        private async void ActionBan_Click(object sender, EventArgs e)
        {
            if (!await User.ChangeUserStatus(_userId, UserStatus.Banned))
            {
                MessageBox.Show("Something went wrong ;<");
                return;
            }
            if (!await ChatMessage.RemoveUserMessages(_userId))
                MessageBox.Show("Something went wrong ;<");
        }

        public void DisplayAdminTools()
        {
            ActionBan.Visible = true;
            ActionRemoveMessage.Visible = true;
        }

        public int GetId()
        {
            return _id;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            if (User.Session.Data.Role == UserRole.Admin)
                DisplayAdminTools();
        }

        public void HideMessage()
        {
            if (!IsHide)
            {
                MessageField.Text = "This message has been deleted by the administrator!";
                IsHide = true;
            }
        }

        public void ShowMessage()
        {
            if (IsHide)
            {
                MessageField.Text = _message;
                IsHide = false;
            }
        }
    }
}
