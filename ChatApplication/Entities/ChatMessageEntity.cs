﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;


namespace ChatApplication.Entities
{
    public class ChatMessageEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        [JsonIgnore]
        public UserEntity User { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [JsonIgnore]
        public ChatEntity Chat { get; set; }
        [Required]
        public Guid ChatId { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(4048)]
        public string Text { get; set; }
        public int AddedAt { get; set; }
        public int DeletedAt { get; set; } = 0;
    }
}
