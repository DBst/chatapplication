﻿using ChatApplication.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbContext = Microsoft.EntityFrameworkCore.DbContext;

namespace ChatApplication.Entities
{
    public class ChatDbContext: DbContext
    {
        private readonly string _connectionString = "Server=BPX-TROJAMAT\\SQLEXPRESS;Database=Chat;Trusted_Connection=True;";

        public DbSet<UserEntity> Users { get; set; }
        public DbSet<UserSessionsEntity> UserSessions { get; set; }
        public DbSet<UserAccessEntity> UserAccesses { get; set; }
        public DbSet<ChatMessageEntity> ChatMessages { get; set; }
        public DbSet<ChatEntity> Chats { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(_connectionString);
        }
    }
}
