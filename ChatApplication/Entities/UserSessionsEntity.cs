﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApplication.Entities
{
    public class UserSessionsEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public Guid UserId { get; set; }
        [JsonIgnore]
        public UserEntity User { get; set; }
        public int CreatedAt { get; set; }
        public int ExpiredAt { get; set; }
    }
}
