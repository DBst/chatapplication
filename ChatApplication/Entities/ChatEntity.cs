﻿using ChatApplication.Forms.Chats;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApplication.Entities
{
    public class ChatEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        [Required]
        [MinLength(3)]
        [MaxLength(35)]
        public string Name { get; set; }
        [Required]
        public ChatType Type { get; set; }
        public ICollection<ChatMessageEntity> Messages { get; set; }
        [JsonIgnore]
        public IEnumerable<UserAccessEntity> ChatUsers { get; set; }
        public byte[] SecretKeyHash { get; set; }
        public byte[] SecretKeySalt { get; set; }
    }
}
