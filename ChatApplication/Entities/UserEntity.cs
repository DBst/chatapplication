﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApplication.Entities
{
    public enum UserStatus
    { 
        Default,
        Banned,
    }

    public enum UserRole
    {
        User,
        Admin,
    }

    public class UserEntity
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public ICollection<ChatMessageEntity> Messages { get; set; }
        [Required]
        [MinLength(4)]
        [MaxLength(50)]
        public string Username { get; set; }
        [Required]
        [MaxLength(150)]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public byte[] PasswordHash { get; set; }
        [Required]
        public byte[] PasswordSalt { get; set; }
        public virtual IEnumerable<UserAccessEntity> UserChats { get; set; }
        public int RecentlyOnlineAt { get; set; }
        [Required]
        public UserStatus Status { get; set; } = UserStatus.Default;
        [Required]
        public UserRole Role { get; set; } = UserRole.User;
    }
}
