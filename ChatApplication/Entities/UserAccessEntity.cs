﻿using ChatApplication.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace ChatApplication.Entities
{
    public class UserAccessEntity
    {
        public Guid Id { get; set; }
        [JsonIgnore]
        public UserEntity User { get; set; }
        [Required]
        public Guid UserId { get; set; }
        [JsonIgnore]
        public ChatEntity Chat { get; set; }
        [Required]
        public Guid ChatId { get; set; }
    }
}
